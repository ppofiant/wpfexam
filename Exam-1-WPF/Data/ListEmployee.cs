﻿using Exam_1_WPF.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_1_WPF.Data
{
    class ListEmployee
    {
        // The core Data will be stored in this List
        public static List<Employee> Employees { get; set; } = new List<Employee>();
        
    }
}
