﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Exam_1_WPF.Model
{
    class Employee
    {
        /*
         * Employee:
         * id -> int
         * name -> string
         * email -> string
         * gender -> string
         * status -> string
         */

        [JsonPropertyName("id")]
        public int Id { get; set; }

        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("email")]
        public string Email { get; set; }

        [JsonPropertyName("gender")]
        public string Gender { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }
    }

    // For API JSON Purpose
    class Book
    {
        // Collect JSON 'data'
        [JsonPropertyName("data")]
        public List<Employee> Data { get; set; }
    }
}
