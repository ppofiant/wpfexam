﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Exam_1_WPF
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        // Initiate ServiceProvider
        private readonly ServiceProvider serviceProvider;
        public App()
        {
            //Initiate Log File Location
            Log.Logger = new LoggerConfiguration()
                           .WriteTo.Console()
                           .WriteTo.RollingFile(@"logs\logWpfExam-{Date}.txt")
                           .CreateLogger();

            Log.Information("App WPF is running!");

            // Implementing Dependecy Injection
            // using Microsoft.Extensions.DependencyInjection;
            ServiceCollection services = new ServiceCollection();
            ConfigureServices(services);
            serviceProvider = services.BuildServiceProvider();
        }

        private void ConfigureServices(ServiceCollection services)
        {
            services.AddHttpClient();
            services.AddSingleton<MainWindow>();
        }

        /*
         * Showing the MainWindow.xaml from App.xaml
         */
        private void OnStartup(object sender, StartupEventArgs e)
        {
            var mainWindow = serviceProvider.GetService<MainWindow>();
            mainWindow.Show();
        }
    }
}
