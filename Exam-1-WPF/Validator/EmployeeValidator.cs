﻿using Exam_1_WPF.Data;
using Exam_1_WPF.Model;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exam_1_WPF.Validator
{
    class EmployeeValidator : AbstractValidator<Employee>
    {
        /*
         * Constructor
         * Validate "Employee"
         */
        public EmployeeValidator()
        {
            //Validation ID
            RuleFor(Q => Q.Id)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .Must(IsUniqueId).WithMessage("The {PropertyName} is already exist!");

            //Validation Name
            RuleFor(Q => Q.Name)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .MinimumLength(5)
                .Must(IsTwoSyllable).WithMessage("The {PropertyName} must be minimum 2 syllable");

            //Validation Email
            RuleFor(Q => Q.Email)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty()
                .EmailAddress()
                .Must(IsDomainFormat).WithMessage("The {PropertyName} must be ended with \'accelist.com\'")
                .Must(IsUniqueEmail).WithMessage("The {PropertyName} is already exist!");
                
            //Validation Gender
            RuleFor(Q => Q.Gender)
                .NotEmpty();
        }

        /*
         * Return bool
         * 
         * @param1 -> id (int)
         * 
         * return true -> nothing matched id
         * return false -> matched id
         */
        private bool IsUniqueId(int id)
        {
            //Using LINQ
            List<Employee> employees = GetList();
            var exist = employees.Where(Q => Q.Id == id).FirstOrDefault();

            //If user is not exist
            if(exist == null)
            {
                return true;
            }
            return false;
        }

        /*
         * Return bool
         * 
         * @param1 -> name (string)
         * 
         * return true -> have 2 words or above
         * return false -> have 1 words only
         */
        private bool IsTwoSyllable(string name)
        {
            // Check by the count of whitespace
            return name.Count(Char.IsWhiteSpace) >= 1;
        }

        /*
         * Return boolean
         * 
         * @param1 -> email (string)
         * 
         * return true -> endsWith "accelist.com"
         * return false -> not endsWith "accelist.com"
         */
        private bool IsDomainFormat(string email)
        {
            return email.EndsWith("accelist.com");
        }

        /*
         * Return bool
         * 
         * @param1 -> email (string)
         * 
         * return true -> nothing matched email
         * return false -> matched email
         */
        private bool IsUniqueEmail(string email)
        {
            List<Employee> employees = GetList();
            var exist = employees.Where(Q => Q.Email.ToLower() == email.ToLower()).FirstOrDefault();

            //If user is not exist
            if (exist == null)
            {
                return true;
            }
            return false;
        }

        private List<Employee> GetList()
        {
            return ListEmployee.Employees;
        }
    }
}
