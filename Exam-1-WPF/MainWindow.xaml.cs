﻿using Exam_1_WPF.Data;
using Exam_1_WPF.Model;
using Exam_1_WPF.Validator;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam_1_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        // Initiate ClientFactory for Depedency Injection Purpose
        private readonly IHttpClientFactory _ClientFactory;
        private readonly HttpClient _Client;

        /*
         * Constructor
         * @param1 -> clientFactory
         * 
         */
        public MainWindow(IHttpClientFactory clientFactory)
        {
            
            InitializeComponent();
            // Initiate Depedency Injection
            _ClientFactory = clientFactory;
            _Client = _ClientFactory.CreateClient();

            //Reference Data grid & ComboxBox
            EmployeeGrid.ItemsSource = ListEmployee.Employees;
            GenerateGender();
            GenerateCategorySearch();
        }

        /*
         * Return Async Void
         * @param1 -> sender (button trigger)
         * @param2 -> e (event Args)
         * 
         * API Fetch button to collect data employee from 
         * url = https://gorest.co.in/public-api/users
         * using JSON and Dependency Injection
         */
        private async void APIButton_Click(object sender, RoutedEventArgs e)
        {
            string url = "https://gorest.co.in/public-api/users";
            var response = await _Client.GetAsync(url);
            try
            {
                response.EnsureSuccessStatusCode();
            } 
            catch (Exception i)
            {
                //Log Error
                MessageBox.Show("Failed Fetch API!");
                Log.Error("Failed Fetch API url: " + url + i.Message);
                return;
            }
            Log.Information("Success fetch API url: " + url);
            
            var content = await response.Content.ReadAsStreamAsync();

            // This is the part that we put insensitive through json
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            // deserialize asynchrounously from json to object
            var data = await JsonSerializer.DeserializeAsync<Book>(content, options);
            // Add data to grid
            AddEmployeToGrid(data.Data.ToList());
        }

        /*
         * Return void 
         * @param1 -> employees (List of Employee)
         * 
         * The function to add the List of employees to DataGrid
         */
        private void AddEmployeToGrid(List<Employee> employees)
        {
            ListEmployee.Employees = employees;
            EmployeeGrid.ItemsSource = ListEmployee.Employees;
            RefreshGrid();
            MessageBox.Show("Success Fetch Employee Data!");
            Log.Information("Added new Employees!");
        }


        /*
         * Return void
         * The function to create value in GenderComboBox
         */
        private void GenerateGender()
        {
            List<String> gender = new List<string> { "Male", "Female" };
            GenderComboBox.ItemsSource = gender;
        }

        /*
         * Return void
         * The function to refresh the data grid
         */
        private void RefreshGrid()
        {
            EmployeeGrid.ItemsSource = ListEmployee.Employees;
            EmployeeGrid.Items.Refresh();
        }

        /*
         * Return void
         * The function to change status of the employee 
         * between Active or Inactive
         */
        private void ChangeStatus(object sender, RoutedEventArgs e)
        {
            var tagToChange = ((Button)sender).Tag.ToString();

            var userStatusChanged = ListEmployee.Employees.Where(Q => Q.Id == Int32.Parse(tagToChange)).FirstOrDefault();
            if(userStatusChanged == null)
            {
                return;
            }
            // If current user status is Active => Inactive
            // Else if current user status is Inactive => Active
            userStatusChanged.Status = userStatusChanged.Status == "Active" ? "Inactive" : "Active";
            RefreshGrid();
        }

        /*
         * Return void
         * The function to Clear the input from textBox
         */
        private void ClearBtn_Click(object sender, RoutedEventArgs e)
        {
            IdTxtBox.Text = null;
            NameTxtBox.Text = null;
            EmailTxtBox.Text = null;
            GenderComboBox.SelectedItem = "Male";
        }

        /*
         * Return void
         * The function to Add new Employee
         * To List and Add to Data Grid
         */
        private void AddEmployeeBtn_Click(object sender, RoutedEventArgs e)
        {
            int id;
            string name, email, gender;

            bool isNumber = Int32.TryParse(IdTxtBox.Text ,out id);
            if(isNumber == false)
            {
                MessageBox.Show("Id must be Number!");
            }

            name = NameTxtBox.Text;
            email = EmailTxtBox.Text;
            gender = GenderComboBox.SelectedItem?.ToString();

            //Starting create object Employee
            Employee newEmployee = new Employee
            {
                Id = id,
                Name = name,
                Email = email,
                Gender = gender,
                Status = "Active"
            };

            //Create Validator
            EmployeeValidator validator = new EmployeeValidator();
            var result = validator.Validate(newEmployee);

            //Clear ErrorList
            ErrorList.Items.Clear();

            if (result.IsValid == false)
            {
                var errors = result.ToString(" , ");
                ErrorList.Items.Add(errors.Replace(" , ", "\n"));
                Log.Error(errors);
            }
            else
            {
                //Input in ListEmployee
                ListEmployee.Employees.Add(newEmployee);
                RefreshGrid();
                MessageBox.Show("Success Add Employee!");
                Log.Information("Success Add New Employee: " + newEmployee.Id);
            }
        }

        /*
         * Return void
         * REGEX Number only
         * The function to avoid the Non Number inputed to TextBox
         */
        private void NumberOnly_PreviewInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        /*
         * Return void
         * The function to generate value of CategoryComboBox
         * Which are : Id, Name, and Email
         */
        private void GenerateCategorySearch()
        {
            List<string> categorySearch = new List<string> { "Id", "Name", "Email" };
            CategorySearchComboBox.ItemsSource = categorySearch;
        }

        /*
         * Return void
         * The function is to search the key from TextBox
         * by the Category that created between:
         * Id, Name, Email
         * 
         */
        private void SearchBtn_Click(object sender, RoutedEventArgs e)
        {
            string category, search;
            category = CategorySearchComboBox.SelectedItem.ToString();
            search = SearchTxtBox.Text;
            if(search == null)
            {
                RefreshGrid();
            }
            switch(category)
            {
                // Search by Id
                case "Id":
                    bool isNumber = Int32.TryParse(search, out int searchId);
                    if(isNumber==false)
                    {
                        MessageBox.Show("The Id must be Number!");
                    } 
                    else
                    {
                        var userById = ListEmployee.Employees.Where(Q => Q.Id == searchId);
                        EmployeeGrid.ItemsSource = userById;
                    }
                    break;

                // Search by Name
                case "Name":
                    string name = search.ToLower();
                    var userByName = ListEmployee.Employees.Where(Q => Q.Name.ToLower().Contains(name));
                    EmployeeGrid.ItemsSource = userByName;
                    break;

                // Search by Email
                case "Email":
                    string email = search.ToLower();
                    var userByEmail = ListEmployee.Employees.Where(Q => Q.Email.Contains(email));
                    EmployeeGrid.ItemsSource = userByEmail;
                    break;
                default:
                    MessageBox.Show("Undefined Category!");
                    break;
            }
        }
    }
}
